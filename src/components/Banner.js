// /* Bootstrap Grid System */
// importing using deconstruction
import {Button, Row, Col, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useContext, useEffect, useState} from 'react';
import Highlights from './Highlights';



export default function Banner(){
    const [productData, setProductData] = useState([]);


    useEffect(() => {
        fetch(`http://localhost:4000/products/activeProducts`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization : `Bearer ${localStorage.getItem(`token`)}`
            }
        }).then(response => response.json())
        .then(data =>{
            setProductData(data.map(product => {
                return (<Highlights key={product._id} product={product}/>)
            }))
        })
    },[])


	return(
		<Container className = "text-center">
			<Row>
				<h4 className = "mt-5"><b>HOT ~ COFFEE</b></h4>
				<p>Get your daily cup of Coffee with our Hottest of the week!</p>
				{productData}
			</Row>
		</Container>
		)
}
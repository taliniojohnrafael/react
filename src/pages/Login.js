


import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Container, Row, Col, FormGroup} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
// importing sweetalert2 
import Swal from 'sweetalert2';

import UserContext from '../UserContext';



export default function Login(){

const [email, setEmail] = useState ('');
const [password, setPassword] = useState ('');
const [isActive, setIsActive] = useState(false);

// const [user, setUser] = useState(null);

//Allows us o consume the User context object and its properties to use for user validation
const {user, setUser} = useContext(UserContext);

//console.log(process.env.REACT_APP_URI)
useEffect(() => {
	if(email !== "" && password !== ""){	
		setIsActive(true);
	}else{
		setIsActive(false);
	}
}, [email, password]);

const navigate = useNavigate();

function loginUser(event){
	event.preventDefault();

	//useEffect(() => {

	fetch(`http://localhost:4000/users/login`, {
		method: "POST",
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password: password
		})
		
	}).then(response => response.json())
	.then(data =>{

		if(data !== false){
			localStorage.setItem('token', data);
			retrieveUserDetails(data);
			//console.log(data);
			
			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to our website!"
			})
			navigate('/');
		}else{
			Swal.fire({
				title:"Authentication failed!",
				icon: "error",
				text: "The email you entered doesn't exists!"
			})
			setPassword('');
			}
			
	})
	//},)


	const retrieveUserDetails = (token) => {
		fetch(`http://localhost:4000/users/profile`,{
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(response=> response.json())
			.then(data => {
				setUser({id: data._id, isAdmin: data.isAdmin})
			})
		
	}
	

	
}

//flex-row-reverse
	return (

		(user.id !== null) ?
			<Navigate to = "/" />
		:
		
		<Container className='w-50'>
		<Row className="d-flex bg-light py-5 my-5 border border-primary">
			<Col className = "col-md-3 col-0 offset-md-1 offset-0">
				<h4 className='text-center'>********</h4>
			</Col>
			<Col className = "col-md-5 col-10 offset-md-2 offset-1">
				<Form onSubmit = {loginUser} className = 'p-4'>
					<h1> Login </h1>



				  	<Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>


				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email" 
				    	value = {email}
				    	onChange = {event => setEmail(event.target.value)}
				    	required/>
				  	</Form.Group>






					<Form.Group className="mb-3" controlId="password" >
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value = {password}
					    	onChange = {event => setPassword(event.target.value)}
					    	required/>
					</Form.Group>
					<FormGroup className="text-center">
						<Button className='px-4' variant="success" type="submit" disabled= {!isActive}>Login</Button>
					</FormGroup>
				  	

				</Form>
			</Col>

		</Row>
	    </Container>
	)
}
